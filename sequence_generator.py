import sys
from random import shuffle

SEPARATOR = " "
output_file = sys.argv[1]
items_length = int(sys.argv[2])
index_to_delete = int(sys.argv[3]) if len(sys.argv) is 4 else None


def write_items_to_file(items_set):
    file = open(output_file, "w")
    first = True

    for item in items_set:

        if not first:
            file.write(SEPARATOR)
        else:
            first = False

        file.write(str(item))

    file.close()


if not output_file:
    print("Missing file output argument")
    sys.exit(0)

items = list(range(items_length))
if index_to_delete:
    del items[index_to_delete]
shuffle(items)

write_items_to_file(items)
