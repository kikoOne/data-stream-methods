#!/usr/bin/env bash

N=1000
OUTPUT_FILE="output.txt"

python3 sequence_generator.py $OUTPUT_FILE $N
python3 find_quartile.py $OUTPUT_FILE $1
