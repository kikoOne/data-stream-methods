#!/usr/bin/env bash

MISSING_ITEM=3
OUTPUT_FILE="output.txt"

for N in `seq 100000 100000 1000000`
do

python3 sequence_generator.py $OUTPUT_FILE $N $MISSING_ITEM
python3 find_missing_item.py $OUTPUT_FILE

done