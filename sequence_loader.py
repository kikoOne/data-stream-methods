SEPARATOR = " "


def get_sequence(input_file):
    with open(input_file, "r") as file:
        file_content = file.read()

    return list(map(int, file_content.split(SEPARATOR)))
