import sys
from random import shuffle

from sequence_loader import get_sequence

N = 100000
success = 0

input_file = sys.argv[1]
k = int(sys.argv[2])

items = get_sequence(input_file)
threshold = int((3 / 4) * len(items))

for n in range(N):
    shuffle(items)

    for i in range(k):
        if items[i] >= threshold:
            success += 1
            break

print("Probability : " + str((N - float(success)) / N))
