# Data streams methods

## Requirements

- python3
- psutil (python3 library)

## Exercise 1

```{bash}
sh exercise_1.sh
```

## Exercise 2

```{bash}
sh exercise_2.sh <k_size>
```

