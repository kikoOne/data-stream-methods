import os
import sys
import time
import psutil
from sequence_loader import get_sequence

TIMES = 10000
input_file = sys.argv[1]


def find_missing_item(items, calculated_sum):
    items_sum = sum(items)
    missing_item = calculated_sum - items_sum

    return missing_item


parsed_items = get_sequence(input_file)
total_sum = (len(parsed_items) * (len(parsed_items) + 1)) / 2

results = []

process = psutil.Process(os.getpid())

time_start = time.clock()
ram_start = process.memory_info().rss

for i in range(TIMES):
    results.append(find_missing_item(parsed_items, total_sum))

time_elapsed = (time.clock() - time_start)
ram_elapsed = process.memory_info().rss - ram_start

lost_item = results[0]
total_length = len(parsed_items) + 1

print(
    "Items: " + str(total_length) + "\tFound: " + str(lost_item) + "\tTime: " + str(time_elapsed) + "\tMemory: " + str(
        ram_elapsed))
